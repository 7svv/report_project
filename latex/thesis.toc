\vspace {-0.5cm}
\nobreakspace {}\hfill \textnormal {หน้า\\}\par 
\select@language {thai}
\contheading 
\contentsline {section}{บทคัดย่อ}{(1)}{table.caption.2}% 
\vspace {\baselineskip }
\contentsline {section}{Abstract}{(2)}{table.caption.3}% 
\vspace {\baselineskip }
\contentsline {section}{กิตติกรรมประกาศ}{(3)}{chapter*.4}% 
\vspace {\baselineskip }
\contentsline {section}{สารบัญ}{(4)}{section*.5}% 
\contentsline {section}{สารบัญรูป}{(7)}{section*.7}% 
\vspace {\baselineskip }
\contentsline {section}{สารบัญตาราง}{(9)}{section*.9}% 
\vspace {\baselineskip }
\addvspace {15\p@ }
\contentsline {chapter}{\numberline {1}บทนำ}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}ที่มาและความสำคัญ}{1}{section.1.1}% 
\contentsline {section}{\numberline {1.2}วัตถุประสงค์}{2}{section.1.2}% 
\contentsline {section}{\numberline {1.3}ขอบเขตการดำเนินงาน}{2}{section.1.3}% 
\contentsline {section}{\numberline {1.4}ขั้นตอนการดำเนินงาน}{3}{section.1.4}% 
\contentsline {section}{\numberline {1.5}ผลที่คาดว่าจะได้รับ}{4}{section.1.5}% 
\contentsline {section}{\numberline {1.6}ตารางการดำเนินงาน}{5}{section.1.6}% 
\addvspace {15\p@ }
\contentsline {chapter}{\numberline {2}ทฤษฎีและงานการศึกษาที่เกี่ยวข้อง}{7}{chapter.2}% 
\contentsline {section}{\numberline {2.1}University Timetabling}{7}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}ไลบรารีตัวแก้ไขข้อจำกัด (Constraint Solver Library)}{8}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}การวิจัย}{9}{subsection.2.1.2}% 
\contentsline {subsection}{\numberline {2.1.3}ตัวอย่างออนไลน์}{10}{subsection.2.1.3}% 
\contentsline {section}{\numberline {2.2}แอนดรอยด์ (Android)}{12}{section.2.2}% 
\contentsline {section}{\numberline {2.3}Android Studio}{13}{section.2.3}% 
\contentsline {section}{\numberline {2.4}ภาษาจาวา (JAVA)}{14}{section.2.4}% 
\contentsline {section}{\numberline {2.5}ไอโอเอส (iOS)}{15}{section.2.5}% 
\contentsline {section}{\numberline {2.6}SQLite (Database)}{17}{section.2.6}% 
\contentsline {section}{\numberline {2.7}Dart}{18}{section.2.7}% 
\contentsline {section}{\numberline {2.8}Flutter}{20}{section.2.8}% 
\contentsline {subsection}{\numberline {2.8.1}Architectural overview: platform channels}{20}{subsection.2.8.1}% 
\addvspace {15\p@ }
\contentsline {chapter}{\numberline {3}การดำเนินงาน / วิธีการวิจัย}{22}{chapter.3}% 
\contentsline {section}{\numberline {3.1}การออกแบบส่วนต่อประสานกับผู้ใช้ (user interface, UI) ของแอปพลิเคชัน}{22}{section.3.1}% 
\contentsline {section}{\numberline {3.2}ศึกษาภาษา Dart และ Flutter สำหรับใช้ในการสร้างแอปพลิเคชัน}{24}{section.3.2}% 
\contentsline {section}{\numberline {3.3}ศึกษาการ Implement UniTime เพื่อดึงตัว Solver มาใช้สำหรับการจัดตาราง}{25}{section.3.3}% 
\addvspace {15\p@ }
\contentsline {chapter}{\numberline {4}ผลการวิจัยและอภิปรายผล}{29}{chapter.4}% 
\contentsline {section}{\numberline {4.1}หน้าแอปพลิเคชันสำหรับการเข้าสู้ระบบ และการสร้างบัญชีผู้ใช้งานใหม่}{29}{section.4.1}% 
\contentsline {section}{\numberline {4.2}หน้าแอปพลิเคชันสำหรับแสดงข้อมูลรายวิชา}{31}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}หน้าสำหรับแสดงข้อมูลรายวิชาทั้งหมด}{31}{subsection.4.2.1}% 
\contentsline {subsection}{\numberline {4.2.2}หน้าสำหรับแสดงข้อมูลรายวิชาที่ผู้ใช้ศึกษาผ่านแล้ว}{33}{subsection.4.2.2}% 
\contentsline {subsection}{\numberline {4.2.3}หน้าสำหรับแสดงข้อมูลรายวิชาที่ผู้ใช้ยังศึกษาไม่ผ่าน}{33}{subsection.4.2.3}% 
\contentsline {section}{\numberline {4.3}หน้าแอปพลิเคชันสำหรับคำนวณการจัดตารางเรียนจากวิชาเรียน}{34}{section.4.3}% 
\contentsline {subsection}{\numberline {4.3.1}ส่วนของการคำนวณความเป็นไปได้ของวิชาที่สามารถลงเรียนได้}{34}{subsection.4.3.1}% 
\contentsline {subsection}{\numberline {4.3.2}ส่วนของตารางเรียนที่ผู้ใช้จะปรับเปลี่ยนตามความต้องการ}{34}{subsection.4.3.2}% 
\contentsline {subsection}{\numberline {4.3.3}ส่วนของตารางเรียนทั้งหมดที่ต้องศึกษาตลอดหลักสูตร}{36}{subsection.4.3.3}% 
\contentsline {section}{\numberline {4.4}หน้าแอปพลิเคชันสำหรับตารางปัจจุบันที่ผู้ใช้เลือกใช้}{37}{section.4.4}% 
\contentsline {section}{\numberline {4.5}หน้าแอปพลิเคชันสำหรับการส่งออกไฟล์ของตารางเป็น PDF}{38}{section.4.5}% 
\contentsline {section}{\numberline {4.6}การออกแบบฐานข้อมูลสำหรับแอปพลิเคชัน}{39}{section.4.6}% 
\contentsline {section}{\numberline {4.7}ทดสอบการสร้างหน้าแอปพลิเคชันด้วยภาษา Dart ใน Flutter}{44}{section.4.7}% 
\contentsline {section}{\numberline {4.8}สร้างหน้าแอปพลิเคชันตามการออกแบบส่วนประสานงานกับผู้ใช้}{45}{section.4.8}% 
\contentsline {section}{\numberline {4.9}ขั้นตอนการทำงานของแอปพลิเคชัน}{49}{section.4.9}% 
\contentsline {subsection}{\numberline {4.9.1}ส่วนที่ 1 ส่วนของการสมัครสมาชิก และการเข้าสู่ระบบ ดังรูปที่ \ref {fig:login_part, register_part}}{49}{subsection.4.9.1}% 
\contentsline {subsection}{\numberline {4.9.2}ส่วนที่ 2 ส่วนของการแสดงรายวิชาที่ยังศึกษาไม่ผ่าน รายวิชาที่ศึกษาผ่านแล้ว และรายวิชาทั้งหมด ดังรูปที่ \ref {fig:allList_page}}{51}{subsection.4.9.2}% 
\contentsline {subsection}{\numberline {4.9.3}ส่วนที่ 3 ส่วนของการคำนวณตารางเรียน และแสดงภาพรวมของตาราง ดังรูปที่ \ref {fig:generate_section}}{54}{subsection.4.9.3}% 
\contentsline {subsection}{\numberline {4.9.4}ส่วนที่ 4 ส่วนของการแสดงภาพรวมของตารางเรียนทั้งหมด ดังรูปที่ \ref {fig:all_timetable_page}}{57}{subsection.4.9.4}% 
\addvspace {15\p@ }
\contentsline {chapter}{\numberline {5}สรุปผลการดำเนินงาน อุปสรรค และการพัฒนาในอนาคต}{59}{chapter.5}% 
\contentsline {section}{\numberline {5.1}สรุปผลดำเนินงาน}{59}{section.5.1}% 
\contentsline {section}{\numberline {5.2}อุปสรรคในการดำเนินงาน}{60}{section.5.2}% 
\contentsline {section}{\numberline {5.3}การนำมาพัฒนาต่อในอนาคต}{61}{section.5.3}% 
\addvspace {15\p@ }
\contentsline {chapter}{รายการอ้างอิง}{62}{section*.75}% 
\contentsfinish 
